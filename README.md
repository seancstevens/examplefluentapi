# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Fluent API Example
* Version 1
* https://seancstevens@bitbucket.org/seancstevens/examplefluentapi.git

### How do I get set up? ###

* Open solution in Visual Studio, there is one console app and 6 class libraries.
* Open Package Manager Console, and one at a time select each class library and run Update-Database.
* A database will be created and populated for each class library in the local db.
* Running the console application will now give a prompt to select the data set created by each library.

### Contribution guidelines ###

* Any code review would be appreciated, here is the layout of the presentation:
* The presentation is made up of the following three areas of code first database creation:
*	1. Properties
*	2. Relationships
*	3. Table Inheritance
* Each area has a library with a data attribute example, and a library with a fluent API example.
* Topics covered in these libraries are as follows:
*	1. Database creation.
*	2. Creating seed data.
*	3. Data access.

### Who do I talk to? ###

* Sean Stevens
* sean.stevens@stgconsulting.com